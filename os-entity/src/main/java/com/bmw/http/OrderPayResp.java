package com.bmw.http;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: han
 * @since: 2020-12-30 16:55
 **/
@Data
public class OrderPayResp implements Serializable {
}
