package com.bmw.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, RedisAutoConfiguration.class})
@EnableAsync(proxyTargetClass = true)
@EnableScheduling
@MapperScan(value = "com.bmw.order.dao")
public class OsOrderApplication {


	public static void main(String[] args) {
		SpringApplication.run(OsOrderApplication.class, args);
	}

}
