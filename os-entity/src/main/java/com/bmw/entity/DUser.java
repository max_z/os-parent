package com.bmw.entity;

import java.util.Date;


/**
 * 用户表
 **/
public class DUser {


	/****/

	private Long id;


	/**
	 * 用户姓名
	 **/

	private String userName;


	/**
	 * 手机号
	 **/

	private String phone;


	/**
	 * 邮箱地址
	 **/

	private String email;


	/**
	 * 创建时间
	 **/

	private Date createTime;


	/**
	 * 更新时间
	 **/

	private Date updateTime;


	/**
	 * 是否删除：0否 1是
	 **/

	private Integer isDeleted;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

}
