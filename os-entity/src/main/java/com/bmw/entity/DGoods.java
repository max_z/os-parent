package com.bmw.entity;

import java.util.Date;


/**
 * 商品表
 **/
public class DGoods {


	/****/

	private Long id;


	/**
	 * 商品名称
	 **/

	private String name;


	/**
	 * 总库存数量
	 **/

	private String count;


	/**
	 * 已售数量
	 **/

	private String saled;


	/**
	 * 品类id（对应品类表，此处不拓展）
	 **/

	private Long categoryId;


	/**
	 * 创建时间
	 **/

	private Date createTime;


	/**
	 * 更新时间
	 **/

	private Date updateTime;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCount() {
		return this.count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getSaled() {
		return this.saled;
	}

	public void setSaled(String saled) {
		this.saled = saled;
	}

	public Long getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
