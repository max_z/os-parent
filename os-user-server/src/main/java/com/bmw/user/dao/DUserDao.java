package com.bmw.user.dao;

import com.bmw.base.bean.CommonQueryBean;
import com.bmw.entity.DUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DUser数据库操作接口类
 **/

@Repository
public interface DUserDao {


	/**
	 * 查询（根据主键ID查询）
	 **/
	DUser selectByPrimaryKey(@Param("id") Long id);

	/**
	 * 删除（根据主键ID删除）
	 **/
	int deleteByPrimaryKey(@Param("id") Long id);

	/**
	 * 添加
	 **/
	int insert(DUser record);

	/**
	 * 修改 （匹配有值的字段）
	 **/
	int updateByPrimaryKeySelective(DUser record);

	/**
	 * list分页查询
	 **/
	List<DUser> list4Page(DUser record, @Param("commonQueryParam") CommonQueryBean query);

	/**
	 * count查询
	 **/
	int count(DUser record);

	/**
	 * list查询
	 **/
	List<DUser> list(DUser record);

}