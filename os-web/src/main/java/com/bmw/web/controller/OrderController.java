package com.bmw.web.controller;

import com.bmw.base.bean.BaseResponse;
import com.bmw.http.*;
import com.bmw.order.service.OrderService;
import com.bmw.vo.OrderVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author: han
 * @since: 2020-12-25 16:50
 **/
@Slf4j
@RequestMapping("/order")
@RestController
public class OrderController {

	@Reference(version = "1.0")
	private OrderService orderService;

	@RequestMapping("/test")
	public Integer test() {
		log.error("Something else is wrong here");
		log.info("开始调用test方法");
		return orderService.test();
	}


	/**
	 * 确认订单页接口
	 *
	 * @param orderConfirmReq
	 * @return
	 */
	public BaseResponse<OrderConfirmResp> orderConfirm(@RequestBody @Valid OrderConfirmReq orderConfirmReq) {

		return BaseResponse.OK;
	}

	/**
	 * 提交订单方法
	 *
	 * @param orderCommitReq
	 * @return
	 */
	public BaseResponse<OrderCommitResp> orderCommit(@RequestBody @Valid OrderCommitReq orderCommitReq) {
		return BaseResponse.OK;
	}

	/**
	 * 支付方法
	 *
	 * @param orderPayReq
	 * @return
	 */
	public BaseResponse<OrderPayResp> orderPay(@RequestBody @Valid OrderPayReq orderPayReq) {
		return BaseResponse.OK;
	}

	/**
	 * 获取订单信息
	 *
	 * @param idReq
	 * @return
	 */
	public BaseResponse<OrderVO> orderInfo(@RequestBody @Valid IdReq idReq) {
		return BaseResponse.OK;
	}
}
