package com.bmw.http;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: han
 * @since: 2020-12-30 16:51
 **/
@Data
public class OrderCommitReq implements Serializable {
}
