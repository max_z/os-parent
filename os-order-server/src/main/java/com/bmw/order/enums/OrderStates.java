package com.bmw.order.enums;

public enum OrderStates {
	// 待支付，待发货，待收货，订单结束
	TO_PAYMENT, TO_DELIVER, TO_RECEIVE, FINISH;
}
