package com.bmw.order.enums;

public enum OrderEvents {
	// 支付，发货，确认收货
	PAYED, DELIVERY, RECEIVED,
}
