package com.bmw.order.dao;

import com.bmw.base.bean.CommonQueryBean;
import com.bmw.entity.DOrderLog;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DOrderLog数据库操作接口类
 **/

@Repository
public interface DOrderLogDao {


	/**
	 * 查询（根据主键ID查询）
	 **/
	DOrderLog selectByPrimaryKey(@Param("id") Long id);

	/**
	 * 删除（根据主键ID删除）
	 **/
	int deleteByPrimaryKey(@Param("id") Long id);

	/**
	 * 添加
	 **/
	int insert(DOrderLog record);

	/**
	 * 修改 （匹配有值的字段）
	 **/
	int updateByPrimaryKeySelective(DOrderLog record);

	/**
	 * list分页查询
	 **/
	List<DOrderLog> list4Page(DOrderLog record, @Param("commonQueryParam") CommonQueryBean query);

	/**
	 * count查询
	 **/
	int count(DOrderLog record);

	/**
	 * list查询
	 **/
	List<DOrderLog> list(DOrderLog record);

}