package com.bmw.goods.dao;

import com.bmw.base.bean.CommonQueryBean;
import com.bmw.entity.DGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DGoods数据库操作接口类
 **/

@Repository
public interface DGoodsDao {


	/**
	 * 查询（根据主键ID查询）
	 **/
	DGoods selectByPrimaryKey(@Param("id") Long id);

	/**
	 * 删除（根据主键ID删除）
	 **/
	int deleteByPrimaryKey(@Param("id") Long id);

	/**
	 * 添加
	 **/
	int insert(DGoods record);

	/**
	 * 修改 （匹配有值的字段）
	 **/
	int updateByPrimaryKeySelective(DGoods record);

	/**
	 * list分页查询
	 **/
	List<DGoods> list4Page(DGoods record, @Param("commonQueryParam") CommonQueryBean query);

	/**
	 * count查询
	 **/
	int count(DGoods record);

	/**
	 * list查询
	 **/
	List<DGoods> list(DGoods record);

}