package com.bmw.entity;

import java.util.Date;


/**
 * 订单进度日志表
 **/
public class DOrderLog {


	/****/

	private Long id;


	/**
	 * 订单id
	 **/

	private Long orderId;


	/**
	 * 订单进度描述
	 **/

	private String desc;


	/**
	 * 创建时间
	 **/

	private Date createTime;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
