package com.bmw.entity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 订单表
 **/
public class DOrder {


	/****/

	private Long id;


	/**
	 * 商品id
	 **/

	private Long goodsId;


	/**
	 * 用户id
	 **/

	private Long userId;


	/**
	 * 订单金额
	 **/

	private BigDecimal orderAmount;


	/**
	 * 订单状态：0.订单打开 1.订单取消 2.订单关闭 3.订单完成
	 **/

	private Integer orderStatus;


	/**
	 * 仓储状态：0.初始状态 1.待分拣 2.待打包 3.待出库 4.已出库
	 **/

	private Integer warehouseStatus;


	/**
	 * 物流状态：0.初始状态 1.待发货 2.待收货 3.已收货 4.退货-商家待收货 5.退货-商家已收货
	 **/

	private Integer transportStatus;


	/**
	 * 支付状态：0.未支付 1.支付成功 2.支付失败 3.待退款 4.已退款
	 **/

	private Integer payStatus;


	/**
	 * 优惠券抵扣金额
	 **/

	private BigDecimal couponAmount;


	/**
	 * 币抵扣金额
	 **/

	private BigDecimal coinAmount;


	/**
	 * 收货地址
	 **/

	private String address;


	/**
	 * 创建时间
	 **/

	private Date createTime;


	/**
	 * 更新时间
	 **/

	private Date updateTime;


	/**
	 * 是否删除：0否 1是
	 **/

	private Integer isDeleted;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGoodsId() {
		return this.goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public BigDecimal getOrderAmount() {
		return this.orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Integer getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Integer getWarehouseStatus() {
		return this.warehouseStatus;
	}

	public void setWarehouseStatus(Integer warehouseStatus) {
		this.warehouseStatus = warehouseStatus;
	}

	public Integer getTransportStatus() {
		return this.transportStatus;
	}

	public void setTransportStatus(Integer transportStatus) {
		this.transportStatus = transportStatus;
	}

	public Integer getPayStatus() {
		return this.payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public BigDecimal getCouponAmount() {
		return this.couponAmount;
	}

	public void setCouponAmount(BigDecimal couponAmount) {
		this.couponAmount = couponAmount;
	}

	public BigDecimal getCoinAmount() {
		return this.coinAmount;
	}

	public void setCoinAmount(BigDecimal coinAmount) {
		this.coinAmount = coinAmount;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

}
