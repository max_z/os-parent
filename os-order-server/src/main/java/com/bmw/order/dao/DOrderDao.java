package com.bmw.order.dao;

import com.bmw.base.bean.CommonQueryBean;
import com.bmw.entity.DOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DOrder数据库操作接口类
 **/

@Repository
public interface DOrderDao {


	/**
	 * 查询（根据主键ID查询）
	 **/
	DOrder selectByPrimaryKey(@Param("id") Long id);

	/**
	 * 删除（根据主键ID删除）
	 **/
	int deleteByPrimaryKey(@Param("id") Long id);

	/**
	 * 添加
	 **/
	int insert(DOrder record);

	/**
	 * 修改 （匹配有值的字段）
	 **/
	int updateByPrimaryKeySelective(DOrder record);

	/**
	 * list分页查询
	 **/
	List<DOrder> list4Page(DOrder record, @Param("commonQueryParam") CommonQueryBean query);

	/**
	 * count查询
	 **/
	int count(DOrder record);

	/**
	 * list查询
	 **/
	List<DOrder> list(DOrder record);

}