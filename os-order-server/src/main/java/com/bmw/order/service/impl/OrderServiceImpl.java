package com.bmw.order.service.impl;

import com.bmw.order.enums.OrderEvents;
import com.bmw.order.enums.OrderStates;
import com.bmw.order.service.OrderService;
import com.bmw.order.statemachine.StateMachine;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: han
 * @since: 2020-12-25 16:22
 **/
@Slf4j
@Service(registry = "dubboRegistry", timeout = 3000, version = "1.0", retries = 0, loadbalance = "random", actives = 5)
public class OrderServiceImpl implements OrderService {

	@Autowired
	StateMachine<OrderStates, OrderEvents> stateMachine;

	@Override
	public Integer test() {
		try {
			OrderStates state = stateMachine.getNextState(OrderStates.TO_PAYMENT, OrderEvents.PAYED);
			log.info(state.name());
		} catch (Exception e) {
			log.error("===excp!===", e);
		}
		return 0;
	}

	
}
