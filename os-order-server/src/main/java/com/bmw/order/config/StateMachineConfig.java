package com.bmw.order.config;

import com.bmw.order.enums.OrderEvents;
import com.bmw.order.enums.OrderStates;
import com.bmw.order.statemachine.StateMachine;
import com.google.common.collect.Sets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: han
 * @since: 2020-12-29 14:19
 **/
@Configuration
public class StateMachineConfig {

	@Bean
	public StateMachine stateMachine() {
		StateMachine stateMachine = new StateMachine(OrderStates.TO_PAYMENT, Sets.newHashSet(OrderStates.FINISH));
		stateMachine.addTransition(OrderStates.TO_PAYMENT,
				OrderStates.TO_DELIVER,
				OrderEvents.PAYED);

		stateMachine.addTransition(OrderStates.TO_DELIVER,
				OrderStates.TO_RECEIVE,
				OrderEvents.DELIVERY);

		stateMachine.addTransition(OrderStates.TO_RECEIVE,
				OrderStates.FINISH,
				OrderEvents.RECEIVED);
		return stateMachine;
	}
}
